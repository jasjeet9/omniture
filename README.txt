This repository holds the s_code.js source files and mechanism to build it into
the version we push live.

Info available here: http://wiki.in.telstra.com.au/display/tls/s_code+build+system

Contact 

Kamal Singh <kamal.singh@team.telstra.com> or
Adam Creek <adam.creek@team.telstra.com> 

for further details.